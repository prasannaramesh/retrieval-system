Aim: To understand various steps involved in the retrieval of documents using Lucene library
Java version: jdk 8 (minimum)

Libraries used, versions and their functionality
lucene-core: 7.7.2 - indexing the documents
lucene-analyzers: 7.7.2 - stemming the words, stopword removal,tokenization of search text
lucene-queryparser: 7.7.2 - creating parsed queries based on the given query text
jsoup: 1.12.1 -  parsing the HTML files

Prerequisites:
jdk 8 or above

To Run
java -jar IR_P01.jar [path to document folder]
sample: java -jar IR_P01.jar "\Home\IR\"