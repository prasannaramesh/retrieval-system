package IR.sages;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.ClassicSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

public class QueryMatcher {

	private Directory indexDirectory;

	private String documentPath;

	private int maxResultCount=10;

	
	//constructor
	public QueryMatcher(String documentPath,String indexPath,int maxResultCount)
	{
		this.maxResultCount=maxResultCount;
		this.documentPath=documentPath;
		try {
			this.indexDirectory=FSDirectory.open(Paths.get(indexPath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//check the index folder and retrieved documents
	public void fetchDocument(String searchText) throws IOException, ParseException
	{
//		System.out.println(maxResultCount);
		IndexReader reader = DirectoryReader.open(indexDirectory);
		IndexSearcher searcher = new IndexSearcher(reader);
		MultiFieldQueryParser queryParser = new MultiFieldQueryParser(new String[]
				{DocumentConstants.TITLE,DocumentConstants.BODY}, 
				new StandardAnalyzer());
		Query query = queryParser.parse(searchText);
		searcher.setSimilarity(new ClassicSimilarity());
		TopDocs retrievedDoc = searcher.search(query, maxResultCount);
		if(retrievedDoc.scoreDocs.length>0)
		{
			int rank=1;
			Initiator.printSymbol();
			System.out.println("\t\t\tRetrieved Documents");
			Initiator.printSymbol();
			//			Document currentDoc;
			for(ScoreDoc score:retrievedDoc.scoreDocs) {
				Document currentDoc = searcher.doc(score.doc);
				System.out.println("File/Title Name: "+currentDoc.get(DocumentConstants.TITLE)
				+"\nLast ModifiedDate: "+ currentDoc.get(DocumentConstants.DATE)
				+"\nRank: "+rank+++"    Relevance Score: "+score.score);
				System.out.println("Path: "+currentDoc.get(DocumentConstants.PATH)+"\n");
			}
		}
		else
		{
			System.out.println("\n\t\t**********No match found**********\n");
		}
		wishToContinue();
		//		Initiator.startProcess("dd");
	}

	//get the user wish
	private void wishToContinue() {
		Initiator.printSymbol();
		System.out.println("Do you wish to search for any other term?\n");
		System.out.println("\"Y\" for Yes and any other key for No");
		Scanner input = new Scanner(System.in);
		String temp=input.next();
		if(temp.toLowerCase().equals("y")) {
			Initiator.startProcess(documentPath);
		}
		else {
			Initiator.printSymbol();
			System.out.print("\t\t\t\tEnd\n");
			Initiator.printSymbol();
		}

	}
}
