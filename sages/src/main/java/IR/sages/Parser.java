package IR.sages;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexFileNames;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.jsoup.Jsoup;

import IR.sages.IndexerAndDataFetcher;
import IR.sages.IndexerAndDataFetcher.indexStructure;

public class Parser {
	//For index directory	
	private Directory indexDirectory;
	
	//Document to be processed. Fed from main
	private Path documentFolderPath;
	
	//Indexed document path. Fed from main
	private Path indexFolderPath;

	/**
	 * Constructor
	 * Assign the path and directory to the global variables
	 */
	
	public Parser(String documentFolderPath,String indexFolderPath) {
		this.documentFolderPath= Paths.get(documentFolderPath);
		this.indexFolderPath= Paths.get(indexFolderPath);
		try {
			this.indexDirectory = FSDirectory.open(this.indexFolderPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//parse the directory and check if the index is available for a given file
	//if index is not avaliable calles indexOneFile with the allowed file types and their contents
	public void startIndexing() throws IOException {
		// Stemming is carried out only for English language
		IndexWriter writer = new IndexWriter(indexDirectory, new IndexWriterConfig(new EnglishAnalyzer()));
		//prevents the error when the index directory is empty 
		writer.commit();
		IndexReader reader = DirectoryReader.open(indexDirectory);
		IndexSearcher searcher = new IndexSearcher(reader);
		IndexerAndDataFetcher iAd = new IndexerAndDataFetcher();
		Files.walk(documentFolderPath).filter(allowedDocument).forEach(path->{
			Term term = new Term(DocumentConstants.PATH,path.toString());
			try {
				if(searcher.search(new TermQuery(term), 1).totalHits==0)
				{
					iAd.indexOneFile.accept(iAd.getData(path), writer);
					
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println("HHH");
		});
		writer.close();
	}
	
	//Check if the file is .txt or .html or .htm
	private Predicate<Path> allowedDocument = path->{
		boolean flag = false;
		String fileName= path.toFile().toString().toLowerCase();
		if(fileName.endsWith(DocumentConstants.TXT_TYPE_DOC)
				||
				fileName.endsWith(DocumentConstants.HTM_TYPE_DOC)
				||
				fileName.endsWith(DocumentConstants.HTML_TYPE_DOC))
			flag = true;
		return flag;
	};
	
}
