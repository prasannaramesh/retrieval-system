package IR.sages;


//this class holds the details of all allowed document extension
public class DocumentConstants {
	
	//Constants are self explanatory
	
	public static final String TXT_TYPE_DOC = ".txt";
	
	public static final String HTM_TYPE_DOC = ".htm";
	
	public static final String HTML_TYPE_DOC = ".html";
	
	public static final String TITLE = "docTitle";
	
	public static final String PATH = "docPath";
	
	public static final String BODY = "docBody";
	
	public static final String DATE = "docDate";
	
}
