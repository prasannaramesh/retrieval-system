package IR.sages;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.function.BiConsumer;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.jsoup.Jsoup;

//import org.jsoup.nodes;

public class IndexerAndDataFetcher {

	//index the given document
	public BiConsumer<indexStructure, IndexWriter> indexOneFile = (indexStructure structure,
			IndexWriter writer)->{
				Document doc = new Document();
				doc.add(new StringField(DocumentConstants.PATH,structure.path, Field.Store.YES));
				doc.add(new StringField(DocumentConstants.TITLE,structure.title.toLowerCase(), Field.Store.YES));
				doc.add(new StringField(DocumentConstants.DATE,structure.date, Field.Store.YES));
				doc.add(new TextField(DocumentConstants.BODY,structure.body, Field.Store.YES));
				try {
					System.out.println("Building index for "+structure.path);
					writer.addDocument(doc);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			};

			//Structure of indexing
			public final class indexStructure{
				String title;
				String path;
				String body;
				String date;
			}

			//gathers the title, path and body of the document
			public indexStructure getData(Path path) throws IOException {
				indexStructure structure = new indexStructure();
				//				FileTime fileTime=Files.getLastModifiedTime(path);
				SimpleDateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				structure.date=dateFormat.format(Files.getLastModifiedTime(path).toMillis());
				structure.path=path.toString();
				if(path.getFileName().toString().toLowerCase().endsWith(DocumentConstants.HTM_TYPE_DOC)
						||
						path.getFileName().toString().toLowerCase().endsWith(DocumentConstants.HTML_TYPE_DOC)) {
					org.jsoup.nodes.Document parsedHTMLDoc=Jsoup.parse(new File(path.toString()),"UTF-8");
					if(parsedHTMLDoc.title() == null || parsedHTMLDoc.title().isEmpty()) {
						structure.title=path.getFileName().toString();
						structure.body=structure.title+parsedHTMLDoc.body().text()+structure.date;
					}
					else {
						structure.title=parsedHTMLDoc.title();
						structure.body=parsedHTMLDoc.title()+parsedHTMLDoc.body().text()+structure.date;
					}
				}
				else {
					structure.title = path.getFileName().toString();
					structure.body=getStringContent(path);
				}

				return structure;
			}

			//Convert body of the file into a string
			private String getStringContent(Path path) {
				StringBuilder content = new StringBuilder();
				try {
					Files.readAllLines(path, StandardCharsets.UTF_8).forEach(line->content.append(line).append("\n"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return content.toString();
			}
}
