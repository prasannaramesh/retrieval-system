package IR.sages;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.flexible.core.nodes.DeletedQueryNode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;


public class Initiator 
{
	public static void main( String[] args )
	{
		if (args.length != 1) {
			System.out.println("Invalid Parameters");
			System.out.println("Format: java -jar IR_P01.jar [document folder path]");
		}
		else {
			startProcess(args[0]);
		}
	}

	public static void startProcess(String inputDirectory) {
		printSymbol();
		System.out.println("\t\t\t Retrieval Sages");
		printSymbol();
		//The data is processed according to the format
		System.out.println("Operating Directory: "+inputDirectory);
		System.out.println("Provide the term to search: ");
		Scanner input = new Scanner(System.in);
		String queryTerm=input.next();
		if(queryTerm.length()!=0)
		{
			int maxCount=maxCountFetcher();
			String documentPath=inputDirectory;
			String indexPath=documentPath+"/IndexFolder_retrievalSages";
			Parser docParser = new Parser(documentPath,indexPath);
			try {
				docParser.startIndexing();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//search the given query
			QueryMatcher queryMatcher = new QueryMatcher(documentPath,indexPath,maxCount);
			try {
				queryMatcher.fetchDocument(queryTerm);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//		new File(indexPath).delete();
		}
		else
		{
			System.out.println("Invalid input");
			startProcess(inputDirectory);
		}
	}
	
	public static void printSymbol() {
		System.out.println("==============================================================");
	}
	
	public static int maxCountFetcher() {
    	int validInteger=0;
    	System.out.println("Enter the maximum number of retrieved documents to be shown: ");
    	java.util.Scanner SS=new java.util.Scanner(System.in);
    	if(SS.hasNextInt())
    		validInteger=SS.nextInt();
    	else
    	{
    		System.out.println("Maximum count can be only integers");
    		return maxCountFetcher();
    	}
    	//SS.close();
		return validInteger;
    }
}
